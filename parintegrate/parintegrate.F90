PROGRAM parintegrate

    ! This program came from my Dropbox slides of CS471 from Autumn 1998

    ! This program assumes that N and the number of PE's will be chosen to
    ! insure that each PE has the same number of rectangles


    IMPLICIT NONE

    ! Suck in the contents of the MPI Fortran header file
    INCLUDE "mpif.h"

    ! Parameters
    DOUBLE PRECISION, PARAMETER :: N=256E+07       ! Total number of rectangles
    DOUBLE PRECISION, PARAMETER :: X0=0.0, Xn=2.0  ! Interval endpoints

    INTEGER :: MyRank,                       &
&              NumProcs,                     &
&              ierr,                         &
&              MPIStatus(MPI_STATUS_SIZE),   &
&              MyN,                          &
&              MessageTag,                   &
&              pe, i,                        &
&              status

    DOUBLE PRECISION :: DeltaX,               &
&                       MyX,                  &
&                       MySum,                &
&                       PartialSum,           &
&                       ExactValue,           &
&                       startsecs, stopsecs

! User-defined functions
    DOUBLE PRECISION :: f,            &     ! Function to integrate
&                       fintegral           ! Integral of f (for comparison)

    CHARACTER(LEN=80) :: hostname

    CALL MPI_INIT(ierr)

    CALL MPI_COMM_RANK(MPI_COMM_WORLD, MyRank, ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD, NumProcs, ierr)

    status = HOSTNM(hostname)

#ifdef DEBUG
    PRINT*, "Hello from MyRank: ", MyRank, " of ", NumProcs, &
&           ".    Hostname: ", hostname
#endif

    startsecs = MPI_WTime()

    ! Set up variables for integration
    DeltaX = DABS(Xn-X0)/N          ! width of rectangles
    IF (MODULO(N, DBLE(NumProcs)) .NE. 0) THEN
        PRINT*, 'MyRank: ', MyRank
        PRINT*, '  N is not evenly divisible by NumProcs: ', N, ',  ', NumProcs
        STOP 
    ENDIF
    MyN = N/NumProcs                ! My number of rectangles 
    MyX = X0 + MyRank*DeltaX*MyN    ! My starting value of x

    ! Calculate local sum
    MySum = 0.0
    DO i=1,MyN
        MySum = MySum + f(MyX)*DeltaX     ! Add area of rectangle to MySum
        MyX = MyX + DeltaX
    ENDDO

    ! Set message tag to some arbitrary value
    MessageTag = 15

    ! Accumulate partial sums
    IF (MyRank .GT. 0) THEN
        ! I am not the master PE, so send my sum to PE 0
        CALL MPI_SEND(MySum, 1, MPI_DOUBLE_PRECISION, 0,     &
&                     MessageTag, MPI_COMM_WORLD, ierr)
    ELSE
        ! I am the master PE, so receive partial sums and add to my own
        DO pe=1,NumProcs-1
            CALL MPI_RECV(PartialSum, 1, MPI_DOUBLE_PRECISION,      &
&                         MPI_ANY_SOURCE, MPI_ANY_TAG,              &
&                         MPI_COMM_WORLD, MPIStatus, ierr)  
#ifdef DEBUG
            WRITE(6,*), 'PE 0 Received: ', PartialSum
#endif

            MySum = MySum + PartialSum
        ENDDO
    ENDIF

    stopsecs = MPI_WTime()

    IF (MyRank .eq. 0) THEN
        WRITE(6,*) '****** Wall time (secs): ', stopsecs-startsecs
    ENDIF

    ! Master prints result, then compares with exact value
    IF (MyRank .EQ. 0) THEN
        ExactValue = fintegral(Xn) - fintegral(X0)
        WRITE(6, 1000) MySum
        WRITE(6, 1010) ExactValue
        WRITE(6, 1020) MySum - ExactValue                  ! Absolute error
        WRITE(6, 1030) (MySum - ExactValue) / ExactValue   ! Relative error
    ENDIF

    CALL  MPI_FINALIZE(ierr)

    ! Formatting statements
1000 FORMAT(1x, 'Calculated Value is ', E15.5)
1010 FORMAT(1x, 'Exact Value is ', E15.5)
1020 FORMAT(1x, 'Absolute Error is ', E15.5)
1030 FORMAT(1x, 'Relative Error is ', E15.5)

END PROGRAM parintegrate

DOUBLE PRECISION FUNCTION f(x)

    IMPLICIT NONE
    DOUBLE PRECISION :: x

    f = x*x*x + 2.0*x*x + 3.0*x - 5

    RETURN

END FUNCTION f    

DOUBLE PRECISION FUNCTION fintegral(x)

    IMPLICIT NONE
    DOUBLE PRECISION :: x

    fintegral = x*x*x*x/4.0 + 2.0*x*x*x/3.0 + 3.0*x*x/2.0 - 5.0*x

    RETURN

END FUNCTION fintegral    


