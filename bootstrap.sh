#!/usr/bin/env bash

echo 'bootstrap.sh provisioning...'
echo '*ENVIRONMENT*'
env
echo '-----------------------------'
apt-get update
apt-get install -y csh gfortran m4 build-essential mpich
apt-get install -y cpp-4.7
apt-get install -y nfs-common

# Add user "hpcuser"
adduser --shell /bin/bash --disabled-password --gecos "HPC User" hpcuser
echo -e "$PASSWORD\n$PASSWORD" | passwd  hpcuser

# Mount the wrf geog EFS filesystem and make sure it's ro for non-root
mkdir /home/hpcuser/wrfgeog
#mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 $(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone).$EFS_WRFGEOG_ID.efs.$AWS_REGION.amazonaws.com:/ /home/hpcuser/wrfgeog
AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
echo $AVAIL_ZONE.$EFS_WRFGEOG_ID.efs.$AWS_REGION.amazonaws.com:/ /home/hpcuser/wrfgeog nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0 >> /etc/fstab
mount -a
chown -R root /home/hpcuser/wrfgeog
chmod 755 /home/hpcuser/wrfgeog



# Allow for password login (necessary for AWS, I think)
echo 'Setting up sshd_config for password access...'
sed "/PasswordAuthentication/ c\PasswordAuthentication yes" -i /etc/ssh/sshd_config
service ssh restart


